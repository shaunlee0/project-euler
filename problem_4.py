"""
    A palindromic number reads the same both ways. The largest palindrome made
    from the product of two 2-digit numbers is 9009 = 91 * 99.

    Find the largest palindrome made from the product of two 3-digit numbers.

    Answer:

"""

largest_palindrome = 1


def is_palindrome(potential_palindrome):
    result = True
    potential_palindrome_str = str(potential_palindrome)
    count = 1
    length = potential_palindrome_str.__len__()
    for j in range(0, length):
        char = potential_palindrome_str[j]
        outer_char = potential_palindrome_str[length - count]

        if char != outer_char:
            return False

        count += 1

    return result


for i in range(999, 100, -1):
    for j in range(999, 100, -1):
        product = i * j
        if is_palindrome(product):
            if product > largest_palindrome:
                largest_palindrome = product

print largest_palindrome
