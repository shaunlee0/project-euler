"""
    The prime factors of 13195 are 5, 7, 13 and 29.

    What is the largest prime factor of the number 600851475143 ?

    Answer: 6857

"""

prime_factors = []
factors = []
total = 600851475143
factorials_equal_total = False
current_divisor = 2
largest_divisor = 0
result = False


def is_factor(to_factor, potential_factor):
    return to_factor % potential_factor == 0


while total != 1:
    if is_factor(total, current_divisor):
        total = total / current_divisor
        largest_divisor = current_divisor
        current_divisor = 2
    else:
        current_divisor += 1

print largest_divisor
